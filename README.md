# AdminLTE-3-Codeigniter-3-User-Management-System-Admin-Panel-Login-Role-Group

**Admin Panel - User Management Demo using CodeIgniter + AdminLTE Bootstrap Theme**

The code is uploaded to demonstrate the simple role based Admin Panel application using CodeIgniter(MVC Framework)

**Purpose :**

For every website, we need some sort of admin panel to monitor over the content of the website. The developers must have to start with the basic functinalities like login, logout, create/manage admin users, manage their roles, change password, forget password etc. This repository gives you all above things readymade as boilerplate for admin panel (but by using CodeIgniter PHP MVC framework). You just start code to add your project feature in it.

## Features
1. Login, Logout.
2. Change Password.
3. Create, Update, Delete Users.
4. Assign Dynamic Roles in Existing Group.
5. Create, Update, Delete Groups.

## Upcoming Features
1. Forget Password.
2. Login history of Users.


## Version Information
**1) Upto Release 1.0.0 -** CodeIgniter 3.1.6, PHP version 7.3 or newer, MySQL (4.1+), MySQLi



<img src="snapshots/Login.png">
<img src="snapshots/Dashboard.png">
<img src="snapshots/ManageUser.png">
<img src="snapshots/AddUser1.png">
<img src="snapshots/AddUser2.png">
<img src="snapshots/ManageGroup.png">
<img src="snapshots/AddGroup.png">
<img src="snapshots/ProfileUser.png">
<img src="snapshots/SettingsUser1.png">
<img src="snapshots/SettingsUser2.png">

Installation
------------
There are multiple ways to install this repository.

#### Download:

Download from [Github](https://github.com/danny007in/AdminLTE-3-Codeigniter-3-User-Management-System-Admin-Panel-Login-Role-Group.git).

#### Using The Command Line:


__Via Git__
- Clone to your machine
```
git clone https://github.com/danny007in/AdminLTE-3-Codeigniter-3-User-Management-System-Admin-Panel-Login-Role-Group.git
```

Open browser; goto [localhost/phpmyadmin](http://localhost/phpmyadmin).

Create a database with name "ums_db" and import the file "ums_db.sql" in that database.

database name: ums_db

Fork OR Copy the remaining code into your root directory:

for example, for windows

**WAMP : c:/wamp/www/ums**

OR

**XAMPP : c:/xampp/htdocs/ums**

Open browser; goto [localhost/ums](http://localhost/ums) and press enter:

The login screen will appear.

To login, I am going to provide the user-email ids and password below.

**System Administrator Account :**

email : admin@admin.com
password : password


Once you logged in with System Administrator account, you can create user or edit previous user if you want.

Documentation
-------------
Coming soon....

Contribution
------------
Contribution are always **welcome and recommended**! Here is how:

- Fork the repository ([here is the guide](https://help.github.com/articles/fork-a-repo/)).
- Clone to your machine ```git clone https://github.com/YOUR_USERNAME/AdminLTE-3-Codeigniter-3-User-Management-System-Admin-Panel-Login-Role-Group.git```
- Create a new branch
- Make your changes
- Create a pull request

#### Contribution Requirements:
- When you contribute, you agree to give a non-exclusive license to AdminLTE.io to use that contribution in any context as we (AdminLTE.io) see appropriate.
- If you use content provided by another party, it must be appropriately licensed using an [open source](http://opensource.org/licenses) license.
- Contributions are only accepted through Github pull requests.
- Finally, contributed code must work in all supported browsers (see above for browser support).

License
-------
AdminLTE-3-Codeigniter-3-User-Management-System-Admin-Panel-Login-Role-Group is an open source project that is licensed under [MIT](http://opensource.org/licenses/MIT).