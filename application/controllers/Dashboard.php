<?php 

class Dashboard extends Admin_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->not_logged_in();

		$this->data['page_title'] = 'Dashboard';
		$this->load->model('model_privacy');
		$this->load->model('model_tessera');
		$this->load->model('model_contratto');
		
		// $this->load->model('model_users');
	}

	/* 
	* It only redirects to the manage category page
	* It passes the total product, total paid orders, total users, and total stores information
	into the frontend.
	*/
	public function index()
	{

		



		// $this->data['total_users'] = $this->model_users->countTotalUsers();

		// $user_id = $this->session->userdata('id');
		// $is_admin = ($user_id == 1) ? true :false;

		// $this->data['is_admin'] = $is_admin;
		$this->render_template('dashboard', $this->data);
	}


	public function show()
	{
		$privacy_data = $this->model_privacy->getPrivacyData();


		
		

		
		
		$this->data['privacy_data'] = $privacy_data;

		$this->render_template('privacy/show', $this->data);

	}


	
	public function show2()
	{
		$tessera_data = $this->model_tessera->getTesseraData();


		
		

		
		
		$this->data['tessera_data'] = $tessera_data;

		$this->render_template('tessera/show', $this->data);

	}


	public function contratto()
	{
		$contratto_data = $this->model_contratto->getcontrattoData();


		
		

		
		
		$this->data['contratto_data'] = $contratto_data;

		$this->render_template('contratto/show', $this->data);

	}

	public function contrattogenerale()
	{
		

		$this->render_template('generale', $this->data);

	}



	

}