<?php 

class Model_contratto extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function getcontrattoData($contrattoId = null) 
	{
		if($contrattoId) {
			$sql = "SELECT * FROM contrattogenerale WHERE id = ?";
			$query = $this->db->query($sql, array($contrattoId));
			return $query->row_array();
		}

		$sql = "SELECT * FROM contrattogenerale ";
		$query = $this->db->query($sql, array(1));
		return $query->result_array();
	}



	public function delete($id)
	{
		$this->db->where('id', $id);
		$delete = $this->db->delete('tessera');
		return ($delete == true) ? true : false;
	}


}