<?php 

class Model_privacy extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function getPrivacyData($privacyId = null) 
	{
		if($privacyId) {
			$sql = "SELECT * FROM privacy WHERE id = ?";
			$query = $this->db->query($sql, array($privacyId));
			return $query->row_array();
		}

		$sql = "SELECT * FROM privacy ";
		$query = $this->db->query($sql, array(1));
		return $query->result_array();
	}



	public function delete($id)
	{
		$this->db->where('id', $id);
		$delete = $this->db->delete('privacy');
		return ($delete == true) ? true : false;
	}


}