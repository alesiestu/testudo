<?php 

class Model_tessera extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

	public function getTesseraData($tesseraId = null) 
	{
		if($tesseraId) {
			$sql = "SELECT * FROM tesseramento WHERE id = ?";
			$query = $this->db->query($sql, array($tesseraId));
			return $query->row_array();
		}

		$sql = "SELECT * FROM tesseramento ";
		$query = $this->db->query($sql, array(1));
		return $query->result_array();
	}



	public function delete($id)
	{
		$this->db->where('id', $id);
		$delete = $this->db->delete('tessera');
		return ($delete == true) ? true : false;
	}


}