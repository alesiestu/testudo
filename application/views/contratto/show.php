

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Modulo 
        <small>Contratto generale</small>
      </h1>
     
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-md-12 col-xs-12">

       


          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Lista utenti firmatari</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="userTable" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Nome</th>
                  <th>Cognome</th>
                  <th>Data di nascita</th>
                  <th>Comune di nascita</th>
                  <th>Codice fiscale</th>
                  <th>Email</th>
                  <th>Telefono</th>
                  <th>Data firma</th>
                  <th>Abbonamento</th>

                  <?php if(in_array('updateUser', $user_permission) || in_array('deleteUser', $user_permission)): ?>
                  <th>Azioni</th>
                  <?php endif; ?>

                  
                </tr>
                </thead>
                <tbody>
                  <?php if($contratto_data): ?>                  
                    <?php foreach ($contratto_data as $k => $v): ?>
                      <tr>
                        <td><?php echo $v['nome']; ?></td>
                        <td><?php echo $v['cognome']; ?></td>
                        <td><?php echo $v['datanascita']; ?></td>
                        <td><?php echo $v['comunenascita']; ?></td>
                        <td><?php echo $v['codicefiscale']; ?></td>
                        <td><?php echo $v['email']; ?></td>
                        <td><?php echo $v['telefono']; ?></td>
                        <td><?php echo $v['datafirma']; ?></td>
                        <td><?php echo $v['abbonamento']; ?></td>

                        <td>
                     
                        <?php if(in_array('deleteUser', $user_permission)): ?>
                            <a href="<?php echo ('http://localhost/modulo3/Contratti/'.$v['cognome'].$v['nome'].'_contrattogen.pdf') ?>"class="btn btn-default"><i class="fa fa-download"></i> Contratto</a>
                          <?php endif; ?>
                          <?php if(in_array('deleteUser', $user_permission)): ?>
                            <a href="<?php echo base_url('privacy/delete/'.$v['id']) ?>" class="btn btn-default"><i class="fa fa-trash"></i></a>
                          <?php endif; ?>
                         
                          
                        </td>
                      
                        

                        <?php if(in_array('updateUser', $user_permission) || in_array('deleteUser', $user_permission)): ?>

                   
                      <?php endif; ?>
                      </tr>
                    <?php endforeach ?>
                  <?php endif; ?>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- col-md-12 -->
      </div>
      <!-- /.row -->
      

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script type="text/javascript">
    $(document).ready(function() {
      $('#userTable').DataTable();

      $("#mainUserNav").addClass('active');
      $("#manageUserNav").addClass('active');
    });
  </script>
