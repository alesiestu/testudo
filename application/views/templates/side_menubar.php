<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link elevation-4">
      <img src="<?php echo base_url(); ?>assets/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">IscrizioniDigitali</span>
    </a>
  <?php if($user_permission): ?>
    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?php echo base_url(); ?>assets/immagini/testudo.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">CrossfitTestudo</a>
        </div>
      </div>
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
         with font-awesome or any other icon font library -->
         <li class="nav-item has-treeview">
  <a href="#" class="nav-link">
    <i class="nav-icon fas fa-file"></i>
    <p>
     Moduli
     <i class="right fas fa-angle-left"></i>
   </p>
 </a>
 <ul class="nav nav-treeview">
  <?php if(in_array('createUser', $user_permission)): ?>
    <li class="nav-item">
     <a href="<?php echo base_url(); ?>dashboard" class="nav-link">
      <i class="far fa-circle nav-icon"></i>
      <p>Privacy & Responsabilità</p>
    </a>
  </li> 
<?php endif; ?>
<?php if(in_array('updateUser', $user_permission) || in_array('viewUser', $user_permission) || in_array('deleteUser', $user_permission)): ?>
<li class="nav-item">
 <a href="<?php echo base_url('modulo2') ?>" class="nav-link">
  <i class="far fa-circle nav-icon"></i>
  <p>Iscrizione/Rinnovo</p>
</a>
</li> 
<?php endif; ?>


<?php if(in_array('updateUser', $user_permission) || in_array('viewUser', $user_permission) || in_array('deleteUser', $user_permission)): ?>
<li class="nav-item">
 <a href="<?php echo base_url('dashboard/contrattogenerale') ?>" class="nav-link">
  <i class="far fa-circle nav-icon"></i>
  <p>Contratto generale</p>
</a>
</li> 
<?php endif; ?>





</ul>
</li>
         
         
         
         
   

<?php if(in_array('createUser', $user_permission) || in_array('updateUser', $user_permission) || in_array('viewUser', $user_permission) || in_array('deleteUser', $user_permission)): ?>
<li class="nav-item has-treeview">
  <a href="#" class="nav-link">
    <i class="nav-icon fas fa-users"></i>
    <p>
     Lista firme
     <i class="right fas fa-angle-left"></i>
   </p>
 </a>
 <ul class="nav nav-treeview">

 <?php if(in_array('updateUser', $user_permission) || in_array('viewUser', $user_permission) || in_array('deleteUser', $user_permission)): ?>
<li class="nav-item">
 <a href="<?php echo base_url('dashboard/show') ?>" class="nav-link">
  <i class="far fa-circle nav-icon"></i>
  <p>Responsabilità & privacy</p>
</a>
</li> 
<?php endif; ?>
  <?php if(in_array('createUser', $user_permission)): ?>
    <li class="nav-item">
     <a href="<?php echo base_url('dashboard/show2') ?>" class="nav-link">
      <i class="far fa-circle nav-icon"></i>
      <p>Iscrizioni</p>
    </a>
  </li> 
<?php endif; ?>


<?php if(in_array('updateUser', $user_permission) || in_array('viewUser', $user_permission) || in_array('deleteUser', $user_permission)): ?>
<li class="nav-item">
 <a href="<?php echo base_url('dashboard/contratto') ?>" class="nav-link">
  <i class="far fa-circle nav-icon"></i>
  <p>Contratto generale</p>
</a>
</li> 
<?php endif; ?>

</ul>
</li>
<?php endif; ?>












<li class="nav-header">Esci dal pannello</li>

<?php if(in_array('viewProfile', $user_permission)): ?>
  
<?php endif; ?>
<?php if(in_array('updateSetting', $user_permission)): ?>
  
<?php endif; ?>

<?php endif; ?>
<!-- user permission info -->
<li class="nav-item">
  <a href="http://localhost/modulo3/Abbonamenti.xlsx" class="nav-link">
    <i class="nav-icon fas fa-download"></i>
    <p>Scarica Excel</p>
  </a>
</li>

<li class="nav-item">
  <a href="<?php echo base_url('auth/logout') ?>" class="nav-link">
    <i class="nav-icon fas fa-sign-out-alt"></i>
    <p>Logout</p>
  </a>
</li>

</ul>
</nav>
<!-- /.sidebar-menu -->
</div>
<!-- /.sidebar -->
</aside>